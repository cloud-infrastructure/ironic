# Copyright (c) 2017 CERN
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from ironic_lib import metrics_utils

from ironic.common import exception
from ironic.drivers import base
from ironic.drivers.modules import ipmitool
from ironic.drivers.modules.redfish import utils as redfish_utils


METRICS = metrics_utils.get_metrics_logger(__name__)


class CernWebConsole(base.ConsoleInterface):
    """A ConsoleInterface that returns BMC URL and credentials."""

    def get_properties(self):
        return {}

    def start_console(self, task):
        pass

    def stop_console(self, task):
        pass

    @METRICS.timer('CernWebConsole.get_console')
    def get_console(self, task):
        """Get the type and connection information about the console."""
        driver_info = ipmitool._parse_driver_info(task.node)
        url = "https://" + task.node.name + "-ipmi.cern.ch"
        username = driver_info['username']
        password = driver_info['password']
        ipmi_version = ('lan'
                        if driver_info['protocol_version'] == '1.5'
                        else 'lanplus')
        command = "ipmitool -I {0} -U {1} -P {2} -H {3}-ipmi.cern.ch " \
                  "mc reset cold".format(ipmi_version, username, password,
                                         task.node.name)
        command_2 = "ipmitool -I {0} -U {1} -P {2} -H {3}-ipmi.cern.ch " \
                    "chassis power status"\
                    .format(ipmi_version, username, password, task.node.name)
        return {'url': url,
                'username': username,
                'password': password,
                'ipmitool_mc_reset_cold': command,
                'ipmitool_chassis_power_status': command_2}

    @METRICS.timer('CernWebConsole.validate')
    def validate(self, task):
        """Validate the Node console info."""

        driver_info = ipmitool._parse_driver_info(task.node)
        if not driver_info['username']:
            raise exception.MissingParameterValue(
                "Missing 'username' parameter in node's driver_info.")

        if not driver_info['password']:
            raise exception.MissingParameterValue(
                "Missing 'password' parameter in node's driver_info.")


class CernWebConsoleRedfish(base.ConsoleInterface):
    """A ConsoleInterface that returns BMC URL and credentials."""

    def get_properties(self):
        return {}

    def start_console(self, task):
        pass

    def stop_console(self, task):
        pass

    @METRICS.timer('CernWebConsoleRedfish.get_console')
    def get_console(self, task):
        """Get the type and connection information about the console."""
        driver_info = redfish_utils.parse_driver_info(task.node)
        url = driver_info['address']
        username = driver_info['username']
        password = driver_info['password']
        system_id = driver_info['system_id']

        power_status = "rf_power_reset.py --user {0} --password {1} "\
                       "--rhost {2} --info".format(username, password, url)
        power_reset = "rf_power_reset.py --user {0} --password {1} "\
                      "--rhost {2} --type".format(username, password, url)
        get_sel = "rf_logs.py --details --user {0} --password {1} "\
                  "--rhost {2}".format(username, password, url)

        return {'url': url,
                'system_id': system_id,
                'username': username,
                'password': password,
                'power_status': power_status,
                'power_reset': power_reset,
                'get_sel': get_sel}

    @METRICS.timer('CernWebConsoleRedfish.validate')
    def validate(self, task):
        """Validate the Node console info."""

        driver_info = redfish_utils.parse_driver_info(task.node)

        if not driver_info['address']:
            raise exception.MissingParameterValue(
                "Missing 'redfish_address' parameter in node's driver_info.")

        if not driver_info['username']:
            raise exception.MissingParameterValue(
                "Missing 'redfish_username' parameter in node's driver_info.")

        if not driver_info['password']:
            raise exception.MissingParameterValue(
                "Missing 'redfish_password' parameter in node's driver_info.")

        if not driver_info['system_id']:
            raise exception.MissingParameterValue(
                "Missing 'redfish_system_id' parameter in node's driver_info.")
