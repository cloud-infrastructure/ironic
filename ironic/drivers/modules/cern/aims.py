# Copyright (c) 2017 CERN
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import re
import socket
import time

from ironic_lib import utils
from landbclient import client as landbclient
from oslo_concurrency import processutils
from oslo_log import log as logging
import requests

from ironic.conf import CONF
from ironic.drivers import utils as driver_utils


LOG = logging.getLogger(__name__)


class AIMSManager:

    @staticmethod
    def _get_kerberos_credentials():
        """Get kerberos token for svcironic

        Renews a kerberos token for svcbare account using the keytab
        stored in /etc/svcbare.keytab
        """

        try:
            _, __ = utils.execute(
                "/usr/bin/kinit -kt /etc/svcbare.keytab svcbare",
                shell=True)
        except processutils.ProcessExecutionError:
            LOG.error("Authenticating as svcbare failed")
            raise

    @staticmethod
    def _remove_suffix(name):
        if name.endswith('.cern.ch'):
            return name[:-8]
        else:
            return name

    @staticmethod
    def _get_ironic_image(image, arch):
        """Set architecture specification to the img"""
        # Extend in case of new architectures
        architectures = ['aarch64', 'x86_64']
        if arch in architectures:
            architectures.remove(arch)
            for arch_to_subtract in architectures:
                image = re.sub(arch_to_subtract, arch, image, flags=re.I)
            if arch not in image.lower():
                image += '-' + arch
        return image

    @staticmethod
    def _get_device_from_ip(addr):
        try:
            client_landb = landbclient.LanDB(username=CONF.cern.landb_username,
                                             password=CONF.cern.landb_password,
                                             host=CONF.cern.landb_hostname,
                                             port=CONF.cern.landb_port,
                                             protocol=CONF.cern.landb_protocol)

            name = client_landb.device_hostname(addr)[0]
            LOG.debug("Resolved %s to %s", addr, name)
            return name
        except Exception as e:
            LOG.error("Failed to resolve ip %s", addr)
            LOG.error(e)
            raise

    @staticmethod
    def register_node(node, mode, image=None, ks_cfg_path=None):
        """Register node in AIMS

        Registers the node in AIMS in order to be able to perform PXE boot
        from the provided image.

        :param node: Ironic "node" object for the machine being registered
        :param mode: One of {conductor|inspector|anaconda} depending which
                     service requests registration
        :param image: Image from which the machine should be booted
        :param ks_cfg_path: Path to the kickstart file
        """

        LOG.info("Starting AIMS registration for node %s", node.uuid)

        AIMSManager._get_kerberos_credentials()

        # get the boot mode from the node's capabilities,
        # arm64 for aarch64 nodes,
        # use 'uefi' mode as the default
        # set architecture specification to the img
        boot_mode = driver_utils.get_node_capability(node, 'boot_mode')
        arch = node.properties.get('cpu_arch')
        if arch == 'aarch64':
            # aims requires boot node to be set to arm64 for aarch64
            boot_mode = 'arm64'
        else:
            if not boot_mode:
                boot_mode = 'uefi'
        LOG.info("Boot mode is %s", boot_mode)
        # get the LanDB device name from the IPMI address in the node object
        ip = node.driver_info['ipmi_address']
        hostname = AIMSManager._get_device_from_ip(ip)

        # compile the AIMS command
        aims_base_command = "{} addhost --hostname {} " \
            "--{} ".format(
                CONF.cern.aims_client, hostname, boot_mode)
        kopts_by_delivery = CONF.cern.kernel_opts_by_delivery
        delivery = node.name.split('-')[0]
        additional_kopts = kopts_by_delivery.get(delivery, '')
        if mode == "conductor":
            image = AIMSManager._get_ironic_image(image, arch)
            ironic_host = "{}:6385".format(
                socket.gethostbyname(socket.gethostname()))
            command = "{} --name {} --kopts 'ipa-api-url=http://{} " \
                      "ipa-ntp-server=ip-time-1.cern.ch {}'".format(
                          aims_base_command, image, ironic_host,
                          additional_kopts)
        elif mode == "inspector":
            image = AIMSManager._get_ironic_image(image, arch)
            ironic_host = "{}:5050/v1/continue".format(
                socket.gethostbyname(socket.gethostname()))
            command = "{} --name {} --kopts 'ipa-inspection-callback-url=" \
                      "http://{} ipa-inspection-collectors=" \
                      "default,extra-hardware,pci-devices {}'".format(
                          aims_base_command, image, ironic_host,
                          additional_kopts)
        elif mode == "anaconda":
            image_properties = node.instance_info.get('image_properties')
            os_distro = image_properties['os_distro'].lower()
            os_major = image_properties['os_distro_major']
            os_minor = image_properties['os_distro_minor']
            image = "{distro}{major}_{major}.{minor}_{arch}".format(
                distro = os_distro, major = os_major,
                minor = os_minor, arch = arch)
            if os_distro == "alma":
                base = "BaseOS"
            else:
                base = "baseos"
            stage2 = "https://linuxsoft.cern.ch/cern" \
                     "/{distro}/{major}.{minor}/{base}/{arch}/os/".format(
                         distro = os_distro, major = os_major,
                         minor = os_minor, base = base, arch = arch)
            response = requests.head(stage2)
            # Check if the stage 2 link correct
            if response.status_code == 200:
                command = "{} --name {} --kickstart {} " \
                        "--kopts 'ipa-ntp-server=ip-time-1.cern.ch " \
                        "inst.stage2={} {}'".format(
                            aims_base_command, image, ks_cfg_path, stage2,
                            additional_kopts)
        else:
            raise Exception("Unknown call-mode for AIMS: {}".format(mode))

        # register the node with AIMS
        LOG.info("AIMS command for node %s: %s", node.uuid, command)
        try:
            utils.execute(command, shell=True)
        except processutils.ProcessExecutionError as e:
            LOG.info("AIMS command failed! Error: %s", e)
            AIMSManager._handle_aims_error(node, hostname, ip, command, str(e))

        LOG.info("Finished AIMS registration for node %s to boot from image "
                 "%s (%s %s)", node.uuid, image, ip, hostname)

    @staticmethod
    def _handle_aims_error(node, hostname, ip, command, aims_error):
        """Parse the error message from AIMS and react to what went wrong

        :node: the node object
        :param hostname: the hostname
        :param ip: the host's IP address
        :param command: the AIMS command which failed
        :param err: the error message from AIMS
        """
        if "already in use by" in aims_error:
            # If the MAC address is already registered under another name,
            # we need to deregister the old name first ...
            LOG.info("AIMS knows different name for %s, "
                     "fixing ...", node.uuid)
            for line in aims_error.splitlines():
                m = re.search('already in use by (.+?) ', aims_error)
                if m:
                    old_hostname = m.group(1)
                    break

            fixcmd = "{} remhost {}".format(
                CONF.cern.aims_client, old_hostname)
            LOG.info("AIMS fix command for node %s: %s", node.uuid, fixcmd)
            utils.execute(fixcmd, shell=True)

            # ... and then retry again (we will raise an exception here if
            # the command fails again)
            LOG.info("AIMS retry command for node %s: %s", node.uuid, command)
            utils.execute(command, shell=True)

        elif "Cannot get device information for" in aims_error \
             or "is not registered with aims" in aims_error:
            # This means we have a race condition and LanDB returned us
            # an old hostname, but the device has already been renamed.
            # We need to wait a moment and try again. This issue has been
            # observed when deleting instances with nova-landb-rename
            # implemented. Did not happen when creating new instances.
            for attempt in range(0, int(CONF.cern.aims_attempts)):
                LOG.info("AIMS race detected for %s", hostname)

                time.sleep(int(CONF.cern.aims_waittime))

                new_hostname = AIMSManager._get_device_from_ip(ip)
                if new_hostname == hostname:
                    # Race not resolved, still getting wrong name, wait...
                    LOG.info("AIMS race NOT resolved between %s and "
                             "%s. Waiting ...", hostname, new_hostname)
                    continue
                LOG.info("AIMS race resolved between %s and "
                         "%s. Retrying.", hostname, new_hostname)

                LOG.info("Removing %s from AIMS.", hostname)
                fixcmd = "{} remhost {}".format(
                    CONF.cern.aims_client, hostname)
                LOG.info("AIMS remove command for node %s: %s",
                         node.uuid, fixcmd)
                utils.execute(fixcmd, shell=True)
                new_command = command.replace(hostname, new_hostname)
                hostname = new_hostname
                LOG.info("New AIMS command for node %s: %s",
                         node.uuid, new_command)
                utils.execute(new_command, shell=True)

        else:
            raise Exception("Unknown AIMS error: {}".format(aims_error))
