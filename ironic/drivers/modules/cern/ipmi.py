# Copyright (c) 2017 CERN
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from ironic_lib import metrics_utils

from ironic.conductor import task_manager
from ironic.drivers.modules import ipmitool

METRICS = metrics_utils.get_metrics_logger(__name__)


class CernIPMIManagement(ipmitool.IPMIManagement):
    @METRICS.timer('CernIPMIManagement.set_boot_device')
    @task_manager.require_exclusive_lock
    def set_boot_device(self, task, device, persistent=False):
        """Set the boot device for the task's node.

        Overrides the parent class call always using persistent=False,
        as in CERN deployment we always want to first boot from network and
        only afterwards from the local drive.

        """
        super(CernIPMIManagement, self).set_boot_device(task, device, False)
