# Copyright (c) 2017 CERN
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from oslo_config import cfg

opts = [
    cfg.StrOpt('deploy_image_name',
               default='', secret=True,
               help='deploy image name'),
    cfg.StrOpt('landb_username',
               default='', secret=True,
               help='landb username'),
    cfg.StrOpt('landb_password',
               default='', secret=True,
               help='landb password'),
    cfg.StrOpt('landb_hostname',
               default='', secret=True,
               help='landb hostname'),
    cfg.StrOpt('landb_port',
               default='443', secret=True,
               help='landb port'),
    cfg.StrOpt('landb_protocol',
               default='https', secret=True,
               help='landb protocol'),
    cfg.StrOpt('aims_waittime',
               default='15', secret=True,
               help='aims waittime'),
    cfg.StrOpt('aims_attempts',
               default='80', secret=True,
               help='aims attempts'),
    cfg.StrOpt('aims_client',
               default='/usr/bin/aims2client', secret=True,
               help='aims client'),
    cfg.DictOpt('kernel_opts_by_delivery',
                default={},
                mutable=True,
                help='A dictionary of lower-case deliveries as keys '
                     'and any delivery specific kernel options '
                     'as values.'),
]


def register_opts(conf):
    conf.register_opts(opts, group='cern')
